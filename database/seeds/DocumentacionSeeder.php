<?php

use Illuminate\Database\Seeder;

class DocumentacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Documentacion::class)->create([
            'date'      => \Carbon\Carbon::create(2018, 01),
            'file_type' => 'nomina',
            'file'      => 'nomina.pdf',
            'title'     => 'Nomina Enero',
            'user_id'   => \App\User::whereEmail('admin@admin.com')->first()->id
        ]);

        factory(\App\Documentacion::class)->create([
            'date'      => \Carbon\Carbon::create(2018, 02),
            'file_type' => 'nomina',
            'file'      => 'nomina.pdf',
            'title'     => 'Nomina Febrero',
            'user_id'   => \App\User::whereEmail('admin@admin.com')->first()->id
        ]);

        factory(\App\Documentacion::class)->create([
            'date'      => \Carbon\Carbon::create(2018, 03),
            'file_type' => 'nomina',
            'file'      => 'nomina.pdf',
            'title'     => 'Nomina Marzo',
            'user_id'   => \App\User::whereEmail('admin@admin.com')->first()->id
        ]);

        factory(\App\Documentacion::class)->create([
            'date'      => \Carbon\Carbon::create(2018, 04),
            'file_type' => 'nomina',
            'file'      => 'nomina.pdf',
            'title'     => 'Nomina Abril',
            'user_id'   => \App\User::whereEmail('admin@admin.com')->first()->id
        ]);

        factory(\App\Documentacion::class)->create([
            'date'      => \Carbon\Carbon::create(2018, 05),
            'file_type' => 'nomina',
            'file'      => 'nomina.pdf',
            'title'     => 'Nomina Mayo',
            'user_id'   => \App\User::whereEmail('admin@admin.com')->first()->id
        ]);

        factory(\App\Documentacion::class)->create([
            'date'      => \Carbon\Carbon::create(2018, 06),
            'file_type' => 'nomina',
            'file'      => 'nomina.pdf',
            'title'     => 'Nomina Junio',
            'user_id'   => \App\User::whereEmail('admin@admin.com')->first()->id
        ]);

        factory(\App\Documentacion::class)->create([
            'date'      => \Carbon\Carbon::create(2018, 07),
            'file_type' => 'nomina',
            'file'      => 'nomina.pdf',
            'title'     => 'Nomina Julio',
            'user_id'   => \App\User::whereEmail('admin@admin.com')->first()->id
        ]);
    }
}
