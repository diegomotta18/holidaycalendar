<?php

namespace App\Http\Controllers;

use App\User;
use App\Nominas;
use Carbon\Carbon;
use App\Documentacion;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class NominasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $nominas = auth()->user()->nominas()->orderBy('date', 'ASC')->get();

        return view('nominas.index', compact('nominas'));
    }

    public function all()
    {
        $nominas = Documentacion::where('file_type', '=', 'nomina')->orderBy('user_id')->orderBy('date', 'ASC')->get();

        return view('nominas.all', compact('nominas'));
    }
}
