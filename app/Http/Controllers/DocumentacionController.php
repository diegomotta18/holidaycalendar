<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Documentacion;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;

class DocumentacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $documentaciones = Documentacion::orderBy('date', 'DESC')->orderBy('user_id')->get();

        return view('documentacion.index', compact('documentaciones'));
    }

    public function create()
    {
        $users = User::all();

        return view('documentacion.create', compact('users'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'date'      => 'required',
            'file_type' => ['required', Rule::notIn(['select'])],
            'file'      => ['required', 'mimes:pdf'],
            'title'     => 'nullable',
            'user_id'   => ['required', Rule::notIn(['select'])],
        ]);

        $type_file = $request->get('file_type');

        if ($request->has('title')) {
            $path = $request->file('file')->storeAs(
                $type_file,
                $request->user()->id.'_'.str_slug($request->get('title')).'.pdf'
            );
        } else {
            $path = $request->file('file')->store($type_file);
        }

        Documentacion::create([
            'date'      => Carbon::createFromFormat('m/Y', $request->get('date')),
            'file_type' => $request->get('file_type'),
            'file'      => $path,
            'title'     => $request->has('title') ? $request->get('title') : null,
            'user_id'   => $request->get('user_id')
        ]);

        return back()->with([
            'message'    => 'Archivo creado correctamente!',
            'alert-type' => 'success',
        ]);
    }

    public function edit($id)
    {
        $doc = Documentacion::findOrFail($id);

        $users = User::all();

        return view('documentacion.edit', compact('users', 'doc'));
    }

    public function update($id, Request $request)
    {
        $doc = Documentacion::findOrFail($id);

        $this->validate($request, [
            'date'      => 'required',
            'file_type' => ['required', Rule::notIn(['select'])],
            'file'      => ['nullable', 'mimes:pdf'],
            'title'     => 'nullable',
            'user_id'   => ['required', Rule::notIn(['select'])],
        ]);

        $doc->date = Carbon::createFromFormat('m/Y', $request->get('date'));
        $doc->file_type = $request->get('file_type');
        $doc->title = $request->has('title') ? $request->get('title') : null;
        $doc->user_id = $request->get('user_id');

        $type_file = $request->get('file_type');

        if ($request->has('title') and $request->has('file')) {
            $path = $request->file('file')->storeAs(
                $type_file,
                $request->user()->id.'_'.str_slug($request->get('title')).'.pdf'
            );

            $doc->file = $path;
        } else {
            if ($request->has('file')) {
                $path = $request->file('file')->store($type_file);
                $doc->file = $path;
            }
        }

        $doc->save();

        return back()->with([
            'message'    => 'Archivo actualizado correctamente!',
            'alert-type' => 'success',
        ]);

    }

    public function download($id)
    {
        $doc = Documentacion::findOrFail($id);

        return response()->download(storage_path('app/'.$doc->file));
    }

    public function destroy($id)
    {
        $doc = Documentacion::findOrFail($id);

        Documentacion::destroy($id);

        return back()->with([
            'message'    => 'Archivo eliminado correctamente!',
            'alert-type' => 'success',
        ]);
    }
}
