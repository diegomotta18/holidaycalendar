<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documentacion extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'date',
        'file_type',
        'file',
        'title',
        'user_id',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
        'date'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    public function getTitleAttribute()
    {
        if(isset($this->attributes['title'])){
            return $this->attributes['title'];
        } else {
            return $this->date->format('m/Y');
        }
    }
}
