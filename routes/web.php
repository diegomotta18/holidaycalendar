<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [
    'uses' => 'HomeController@index',
    'as'   => 'home'
]);

Route::get('/home', function () {
    return redirect()->route('home');
});

/**
 * Vacaciones
 */
Route::post('vacaciones/store', 'VacacionesController@store')->name('vacaciones.store');
Route::post('vacaciones/storeb', 'VacacionesController@storeb')->name('vacaciones.storeb');

Route::get('vacaciones', 'VacacionesController@index')->name('vacaciones.index');
Route::get('vacacionesdt', 'VacacionesController@getVacaciones');
Route::get('vacaciones/user/{id}', 'HomeController@getVacacionesPorUsuario')->name('vacaciones.foruser');
Route::get('vacaciones/users', 'HomeController@getAllVacaciones')->name('vacaciones.foralluser');
Route::post('vacaciones/approve/{id}', 'VacacionesController@approveVacaciones')->name('vacaciones.approve');
Route::post('vacaciones/deny/{id}', 'VacacionesController@denyVacaciones')->name('vacaciones.deny');
Route::post('vacacionesdtall', 'VacacionesController@getVacacionesAllDt');
Route::post('vacacionesdtdeny', 'VacacionesController@getVacacionesDenyDt');
Route::post('vacacionesdtapprove', 'VacacionesController@getVacacionesApproveDt');
Route::post('vacacionesdtnotapprove', 'VacacionesController@getVacacionesNotApproveDt');
Route::delete('/vacaciones/{id}', 'VacacionesController@destroy');
Route::post('/vacacionesforuseryear', 'VacacionesController@getVacacionesForUser');


/**
 * Viajes
 */
Route::get('viajes', 'ViajesController@index')->name('viajes.index');
Route::get('viajes/create', 'ViajesController@create')->name('viajes.create');
Route::get('viajes/{id}', 'ViajesController@show')->name('viajes.show');
Route::post('viajes/store', 'ViajesController@store')->name('viajes.store');
Route::get('viajesdt', 'ViajesController@getViajes');
Route::post('viajes/approve/{id}', 'ViajesController@approveViaje')->name('vacaciones.approve');
Route::post('viajes/deny/{id}', 'ViajesController@denyViaje')->name('vacaciones.deny');
Route::get('viajesdtall', 'ViajesController@getViajesAllDt');
Route::get('viajesdtdeny', 'ViajesController@getViajesDenyDt');
Route::get('viajesdtapprove', 'ViajesController@getViajesApproveDt');
Route::get('viajesdtnotapprove', 'ViajesController@getViajesNotApproveDt');
/**
 * Nominas
 */
Route::get('nominas', 'NominasController@index')->name('nominas.index');

/**
 * Documentacion
 */
Route::get('documentacion', 'DocumentacionController@index')->name('documentacion.index');
Route::get('documentacion/create', 'DocumentacionController@create')->name('documentacion.create');
Route::post('documentacion/store', 'DocumentacionController@store')->name('documentacion.store');
Route::get('documentacion/{id}/edit', 'DocumentacionController@edit')->name('documentacion.edit');
Route::put('documentacion/{id}/update', 'DocumentacionController@update')->name('documentacion.update');
Route::get('documentacion/descargar/{id}', 'DocumentacionController@download')->name('documentacion.download');
Route::delete('documentacion/destroy/{id}', 'DocumentacionController@destroy')->name('documentacion.destroy');

/**
 * Beneficios
 */
Route::get('/beneficios', 'BeneficiosController@index')->name('beneficios.index');
Route::get('/beneficios/create', 'BeneficiosController@create')->name('beneficios.create');
Route::post('/beneficios/store', 'BeneficiosController@store')->name('beneficios.store');
Route::put('/beneficios/{id}/update', 'BeneficiosController@update')->name('beneficios.update');
Route::delete('/beneficios/{id}', 'BeneficiosController@delete')->name('beneficios.delete');
Route::get('/usersj', 'BeneficiosController@usersj');

/**
 * Usuarios
 */
Route::get('/usuarios', 'UserController@index')->name('users.index');
Route::get('/usuarios/create', 'UserController@create')->name('users.create');
Route::post('/usuarios/store', 'UserController@store')->name('users.store');
Route::get('/usuarios/{user}/edit', 'UserController@edit')->name('users.edit');
Route::put('/usuarios/{user}', 'UserController@update')->name('users.update');
