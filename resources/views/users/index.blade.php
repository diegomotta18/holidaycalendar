@extends('layouts.app')
@section('content')
    <div class="page-header">
        <h1 class="page-title">Usuarios</h1>
    </div>

    <div class="page-content container-fluid">
        <div class="row">

            <div class="col-12">

                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Rol</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->getRoleNames()->implode(', ') }}</td>
                                <td>
                                    <div class="form-inline">
                                        <a class="btn btn-sm btn-outline-secondary" style="margin-right: 10px;"
                                           href="{{ route('users.edit', $user->id) }}">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>

            <div class="site-action" data-plugin="actionBtn">
                <a href="{{ route('users.create') }}"
                   class="site-action-toggle btn-raised btn btn-success btn-floating">
                    <i style="margin-top: 9px;" class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
                    <i class="back-icon wb-trash animation-scale-up" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('global/fonts/font-awesome/font-awesome.min.css')}}">
@endpush