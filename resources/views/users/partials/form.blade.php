<div class="row">
    <div class="mx-auto" style="width:600px">

        <div class="form-group">
            <label for="name">Nombre</label>
            {!! Form::text('name', isset($user) ? $user->name :  '', [ 'class' => 'form-control', 'required' ]) !!}
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            {!! Form::email('email', isset($user) ? $user->email :  '', [ 'class' => 'form-control', 'required' ]) !!}
        </div>

        @if(!isset($user))
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" class="form-control">
            </div>
            <div class="form-group">
                <input type="password" name="password_confirmation" class="form-control"
                       placeholder="Confirmar contraseña">
            </div>
        @endif

        <div class="form-group">
            <label for="role">Rol</label>
            {!! Form::select(
                'role',
                $roles ,
                isset($user) ? $user->roles->first()->name :  '',
                [ 'class' => 'form-control', 'required' ])
             !!}
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-block btn-primary">
                Guardar
            </button>
        </div>
    </div>
</div>