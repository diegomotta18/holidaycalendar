@extends('layouts.guest')

@section('content')
    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
        <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
            <div class="brand">
                <img class="brand-img" src="{{asset('topbar/assets/images/logo.png')}}" alt="...">
                <h2 class="brand-text">Jabbar</h2>
                <h3 class="brand-text">Registrarse</h3>

            </div>
            <form method="POST" action="{{ route('register') }}">
                {{ csrf_field() }}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                    <label for="name" class="sr-only">Nombre</label>
                    <input id="name" type="text" class="form-control" placeholder="Nombre" name="name" value="{{ old('name') }}" required
                           autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="sr-only">E-Mail</label>

                    <input id="email" type="email" class="form-control" placeholder="E-mail" name="email" value="{{ old('email') }}"
                           required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="sr-only">Contraseña</label>
                    <input id="password" type="password" class="form-control" name="password" required placeholder="Contraseña">

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>

                <div class="form-group">
                    <label for="password-confirm" class="sr-only">Confirmar contraseña</label>
                    <input id="password-confirm" type="password" class="form-control" placeholder="Confirmar contraseña" name="password_confirmation"
                           required>
                </div>

                <button type="submit" class="btn btn-primary btn-block">
                    Registrar
                </button>
            </form>
        </div>

@endsection
