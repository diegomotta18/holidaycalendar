@extends('layouts.app')
@section('content')
    <div class="page-header row">
        <h1 class="page-title">Solicitud de viajes</h1><div class="btn-group" style="margin-left: 30px;"> <button type="button" id="approve" class="approve btn btn-outline btn-warning">Aprobar</button><button id="denny" type="button" class="denny btn btn-outline btn-danger">Denegar</button></div>
    </div>
    <div class="page-content">
        <div class="panel">
            <div class="panel-body container-fluid">
                <div class="row">
                    <div class="col-md-6">
                        <!-- Example Basic Form (Form grid) -->
                        <div class="example-wrap">
                            <h4 class="example-title">Solicitud de viajes de
                                <b>{{$personas[0]['nombre']." ".$personas[0]['apellido']}}</b></h4>
                            <div class="example">
                                <form autocomplete="off">
                                    @if(count($personas) >1  )
                                        <label class="form-control-label"><b>Acompañantes: {{$viaje->num_personas}}</b></label>
                                        <hr>
                                        @foreach($personas as $key=> $persona)
                                            @if($key != 0)

                                                <div class="row">
                                                    <div class="form-group form-material col-md-6">
                                                        <label class="form-control-label"
                                                               for="inputBasicFirstName">Nombre </label>
                                                        <input type="text" class="form-control"
                                                               value="{{$persona['nombre']}}" id="inputBasicFirstName"
                                                               name="inputFirstName" placeholder="First Name"
                                                               autocomplete="off" disabled>
                                                    </div>
                                                    <div class="form-group form-material col-md-6">
                                                        <label class="form-control-label"
                                                               for="inputBasicFirstName">Apellido</label>
                                                        <input type="text" class="form-control" id="inputBasicFirstName"
                                                               name="inputFirstName" placeholder="First Name"
                                                               autocomplete="off" value="{{$persona['apellido']}}"
                                                               disabled>
                                                    </div>

                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                    <label class="form-control-label"><b>Fecha exacta:</b></label>
                                    <hr>

                                    <div class="row">
                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Fecha de ida</label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName" placeholder="First Name"
                                                   autocomplete="off" value="{{$viaje->fecha_desde}}"
                                                   disabled>
                                        </div>
                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Fecha de vuelta</label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName" placeholder="First Name"
                                                   autocomplete="off" value="{{$viaje->fecha_hasta}}"
                                                   disabled>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Localización</label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName" placeholder="First Name"
                                                   autocomplete="off" value="{{$viaje->localizacion}}"
                                                   disabled>
                                        </div>
                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Destino</label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName" placeholder="First Name"
                                                   autocomplete="off" value="{{$viaje->destino}}"
                                                   disabled>
                                        </div>
                                    </div>
                                    @if(!is_null($viaje->rango_fecha_desde) && !is_null($viaje->rango_fecha_hasta) )
                                        <label class="form-control-label"><b>Rango de fechas:</b></label>
                                        <hr>
                                        <label>Si tienes que viajar pero no tienes cerrada una fecha en
                                            concreto indica de qué dias a qué dias podemos buscar
                                            disponibilidad</label>
                                        <div class="row">

                                            <div class="form-group form-material col-md-6">
                                                <label class="form-control-label"
                                                       for="inputBasicFirstName">Desde</label>
                                                <input type="text" class="form-control" id="inputBasicFirstName"
                                                       name="inputFirstName" placeholder="First Name"
                                                       autocomplete="off" value="{{$viaje->rango_fecha_desde}}"
                                                       disabled>
                                            </div>
                                            <div class="form-group form-material col-md-6">
                                                <label class="form-control-label"
                                                       for="inputBasicFirstName">Hasta</label>
                                                <input type="text" class="form-control" id="inputBasicFirstName"
                                                       name="inputFirstName" placeholder="First Name"
                                                       autocomplete="off" value="{{$viaje->rango_fecha_hasta}}"
                                                       disabled>
                                            </div>
                                        </div>
                                    @endif

                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="example-wrap">
                            <div class="example">
                                <form>
                                    <div class="row">

                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Hotel</label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName" placeholder="First Name"
                                                   autocomplete="off" @if($viaje->hotel ==1) value="Si" @else value="No"
                                                   @endif
                                                   disabled>
                                        </div>
                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Traslado </label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName" placeholder="First Name"
                                                   autocomplete="off" @if($viaje->traslados ==1) value="Si"
                                                   @else value="No" @endif
                                                   disabled>
                                        </div>
                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Coche</label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName" placeholder="First Name"
                                                   autocomplete="off" @if($viaje->coche ==1) value="Si" @else value="No"
                                                   @endif
                                                   disabled>
                                        </div>
                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Tren</label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName" placeholder="First Name"
                                                   autocomplete="off" @if($viaje->tren ==1) value="Si" @else value="No"
                                                   @endif
                                                   disabled>
                                        </div>
                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Avión</label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName"
                                                   autocomplete="off" @if($viaje->avion == 1) value="Si"
                                                   @else value="No" @endif
                                                   disabled>
                                        </div>


                                    </div>
                                    <label class="form-control-label"><b>Fechas de hotel:</b></label>
                                    <hr>
                                    <div class="row">

                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Desde</label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName"
                                                   autocomplete="off" value="{{$viaje->hotel_fecha_desde}}"
                                                   disabled>
                                        </div>
                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Hasta</label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName"
                                                   autocomplete="off" value="{{$viaje->hotel_fecha_hasta}}"
                                                   disabled>
                                        </div>
                                    </div>
                                    <label class="form-control-label"><b>Fechas de coche:</b></label>
                                    <hr>
                                    <div class="row">

                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Desde</label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName"
                                                   autocomplete="off" value="{{$viaje->coche_fecha_desde}}"
                                                   disabled>
                                        </div>
                                        <div class="form-group form-material col-md-6">
                                            <label class="form-control-label"
                                                   for="inputBasicFirstName">Hasta</label>
                                            <input type="text" class="form-control" id="inputBasicFirstName"
                                                   name="inputFirstName"
                                                   autocomplete="off" value="{{$viaje->coche_fecha_hasta}}"
                                                   disabled>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
    $(document).ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var id = {{ $viaje->id }}

        $(".page-header #approve").click(function (e) {
            $.ajax({
                method: 'post',
                url: '/viajes/approve/' + id,
                success: function (response) {
                },
                error: function (error) {
                    console.log(error);
                },

            }).done(function () {
                toastr.options.timeOut = 5000;
                toastr.options.closeButton = true;
                toastr.success("Se ha aprobado el periodo de viajes");
                setTimeout(function(){     window.history.back();
                    ; }, 500);
            });

        });

        $(".page-header #denny").click(function (e) {
            $.ajax({
                method: 'post',
                url: '/viajes/deny/' + id,
                success: function (response) {
                },
                error: function (error) {
                    console.log(error);
                },
            }).done(function () {
                toastr.options.timeOut = 5000;
                toastr.options.closeButton = true;
                toastr.error("Se ha denegado el periodo de viajes");
                setTimeout(function(){     window.history.back();
                    ; }, 500);

            });

        });
    });
</script>
@endpush