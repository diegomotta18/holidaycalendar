@extends('layouts.app')
@section('content')
    <div class="page-header">
        <h1 class="page-title">Documentación</h1>
    </div>
    <div class="page-content container-fluid">
        <div class="row">

            <div class="col-12">

                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>Año</th>
                            <th>Usuario</th>
                            <th>Tipo de archivo</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($documentaciones as $doc)
                            <tr>
                                <td>{{ $doc->date->format('m/Y') }}</td>
                                <td>{{ $doc->user->name }}</td>
                                <td>{{ $doc->file_type }}</td>
                                <td>
                                    <div class="form-inline">
                                        <a class="btn btn-sm btn-outline-secondary" style="margin-right: 10px;"
                                           href="{{ route('documentacion.edit', $doc->id) }}">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                        </a>
                                        <a class="btn btn-sm btn-outline-info" style="margin-right: 10px;"
                                           href="{{ route('documentacion.download', $doc->id) }}">
                                            <i class="fa fa-download" aria-hidden="true"></i>
                                        </a>
                                        <form method="POST" action="{{ route('documentacion.destroy', $doc->id) }}">
                                            {!! csrf_field() !!}
                                            {!! method_field('delete') !!}

                                            <button class="btn btn-sm btn-danger">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="site-action" data-plugin="actionBtn">
                <a href="{{ route('documentacion.create') }}"
                   class="site-action-toggle btn-raised btn btn-success btn-floating">
                    <i style="margin-top: 9px;" class="front-icon wb-plus animation-scale-up" aria-hidden="true"></i>
                    <i class="back-icon wb-trash animation-scale-up" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('global/fonts/font-awesome/font-awesome.min.css')}}">
@endpush